import maya.cmds as cmds


obj_dic = {}
data_pos_dic = {}
data_rot_dic = {}
data_sca_dic = {}
array_data_dic = {}

sel_objects = cmds.ls(sl=True)


# - - - - - - - - - - - - - - - - -
# SELECT DATA AND STORE IN obj_dic
# - - - - - - - - - - - - - - - - -

# write a function selectionData() that gather data from the maya scene
# and returns a dictionary

def selectionData():
    for x in range(0, len(sel_objects)):
        obj_dic.update({"obj_" + str(x): sel_objects[x]})


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# SET DATA AND STORE IN data_pos_dic, data_rot_dic, data_sca_dic
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# write a function setData(data) that takes as input a dictionary previously
# built with selectionData() and store the data in the dictionary to the
# maya objects
def setData():
    for x in range(0, len(sel_objects)):

        data_pos = cmds.getAttr(sel_objects[x] + ".translate")
        data_pos_dic.update({"obj_" + str(x): data_pos[0]})

        data_rot = cmds.getAttr(sel_objects[x] + ".rotate")
        data_rot_dic .update({"obj_" + str(x): data_rot[0]})

        data_sca = cmds.getAttr(sel_objects[x] + ".scale")
        data_sca_dic.update({"obj_" + str(x): data_sca[0]})


# - - - - - - - - - - - - - - - - - - - - - -
# TAKES ALL DATA AND STORE IN array_data_dic
# - - - - - - - - - - - - - - - - - - - - - -

# now rewrite the selectionData() and setData() but rather then saving data of
# a specific object like an array, save each attribute value in a
# subdictionary example:

def array_data():
    for x in range(0, len(sel_objects)):
        name_obj = "obj_" + str(x)
        array_data_dic.update({obj_dic[name_obj]: {"r": data_rot_dic[name_obj],
                                                   "s": data_sca_dic[name_obj],
                                                   "t": data_pos_dic[name_obj]
                                                   }})
    print array_data_dic


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# CALL FUNCTIONS
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
selectionData()
setData()
array_data()


# MY RESULTS

# {u'locator2': {'s': (1.0, 1.0, 1.0),
#                'r': (2.085963662321158, 0.7746448439697666, 19.455644352994266),
#                't': (-3.3085399733895997, 5.059490012084559, 4.67349642594971)},
#  u'locator1': {'s': (1.0, 1.0, 1.0),
#                'r': (48.439149444175634, -37.112101154790196, -7.900701908264064),
#                't': (7.559862451129723, 3.2356993067418687, 0.6104930332123654)},
#  u'pCylinder1': {'s': (1.0, 1.0, 1.0),
#                  'r': (34.05497792785998, 8.062240104433762, -51.19161215648422),
#                  't': (4.858868886582258, 5.670493444077918, 6.41340284324937)}}
