
""""
HOMEWORK CLASS 1 - jRomero
----------------

[x] platform
[x] platform full name 
[x] system version
[x] processor type
[x] python build
[x] user name

"""
import sys
import os
import platform
import getpass



#the first try
print "My plarform is : ", sys.platform
print "My OS is : ", platform.platform()
print "My System version is : ", platform.mac_ver()
print "My processor type is : ", platform.processor()
print "My python build is : ",platform.python_build()
userName = os.uname()
print "My user name is : ", userName[1]



#the second try
hw1_command = {
    "platform": sys.platform, "OS": platform.platform(), "system": platform.mac_ver(),
    "processor": platform.processor(), "python": platform.python_build(), "user": getpass.getuser()}

hw1_text = {"platform": "My plarform is : ", "OS": "My OS is: ", "system": "My system version is: ",
            "processor": "My processor type is: ", "python": "My python build is : ", "user": "My user name is: "}

for i in hw1_command:
    print hw1_text[i], hw1_command[i]
    print ""
    print "-"*60


#The professor solution

plat = sys.platform
name = platform.platform()
ver = platform.mac_ver()
processor = platform.machine()
pyBuild = platform.python_build()
user = getpass.getuser()

data = [plat, name, ver, processor, pyBuild, user]
print "----------- OS DATA ----------"
print data



#screen eg.
import datetime

time = str(datetime.datetime.now().time())
temp = time.split(":")
temp2 = temp[2].split(".")

hours = temp[0]
minutes = temp[1]
seconds = temp2[0]
milli = temp2[1]

print "---------- CURRENT TIME ---------"
print "hours : ", hours
print "minutes : ", minutes
print "seconds : ", seconds
print "milli : ", milli
