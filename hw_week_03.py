import math

list1 = [1, 3, "x", None, 12, 4, 5, 6, 0, 27, 2, 3, 12, 4, 5,
         12, "y", "x", None, True, True, False, 0.11223, math.pi]


##############################################################################
# Filter the list for removing duplicates and creating a new list called noDups

noDups = []


def removeDups(lista):
    for i in lista:
        if i not in noDups:
            noDups.append(i)
    return noDups

removeDups(list1)
print"Values without duplicates in list1:  %s " % noDups
print "- " * 60
print ""

##############################################################################
# filter only the numbers and create a new list called onlyNumbers

onlyNumbers = []


def putNumbers(lista):
    for i in lista:
        if (type(i) == int) or (type(i) == float):
            onlyNumbers.append(i)
    return onlyNumbers

putNumbers(noDups)
print"Numbers without duplicates in list1:  %s " % onlyNumbers
print "- " * 60
print ""

##############################################################################
# do all filtering in a single run and create a list result

result = []


def filterResult(lista):
    for i in lista:
        if i not in result:
            result.append(i)
        elif (type(i) == int) or (type(i) == float):
            result.append(i)
    return result

filterResult(list1)
print"All filtering in a single run:  %s " % filterResult
