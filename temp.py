'''
created on February 2, 2016
@author Jaime
'''

# methods eg.


def printGreetings(greetings="Hello",
                   question="How are you?"):
    print greetings
    print question

print printGreetings("howdy")
print "- - " * 10


def addNumbers(numb1, numb2):
    return numb1 + numb2

print addNumbers(14, 17)
print "- - " * 10


def squareNumber(num):
    global numb3
    numb3 = num * num
    return numb3

print squareNumber(123)
print "- - " * 10


# scope of a function...


numb3 = 20
print numb3  # still the var have the value
print squareNumber(numb3)  # now the var have the new value
print "- - " * 10


def evenOdd(num):
    res = float(num) % 2

    if res == 0:
        return "even"
    else:
        return "odd"

numbs = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]


def checkMultipleEvenOddA(listNumbers):
    for n in range(len(numbs)):
        print evenOdd(numbs[n])


checkMultipleEvenOddA(numbs)
print "- - A" * 10


def checkMultipleEvenOddB(listNumbers):

    count = 0
    while (count < len(listNumbers)):
        print evenOdd(numbs[count])
        count += 1

checkMultipleEvenOddB(numbs)
print "- - B" * 10


def checkMultipleEvenOddC(listNumbers):

    for n in range(len(listNumbers)):
        if type(numbs[n]).__name__ == "float" or type(numbs[n]).__name__ == "int":
            print evenOdd(numbs[n])
        else:
            print "WARNING : expected a float"

checkMultipleEvenOddC(numbs)
print "- - C" * 10
